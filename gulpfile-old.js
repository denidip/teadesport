var gulp = require('gulp');
var shell = require('gulp-shell');
var changed = require('gulp-changed');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var minifycss = require('gulp-minify-css');
var jsmin = require('gulp-jsmin');
var image = require('gulp-image');
var autoprefixer = require('gulp-autoprefixer');
var fileinclude = require('gulp-file-include');
var watch = require('gulp-watch');
var server = require('gulp-webserver');
var $ = require('gulp-load-plugins')();

var sassPaths = [
    'node_modules/foundation-sites/scss',
    'node_modules/motion-ui/src',
    'bower_components/animate.css/'
];
var globs = {
    sass: 'src/style/**/*.scss',
    templates: 'src/templates/**/*.html',
    assets: 'src/assets/**/*.*',
    app: 'src/app/**/*.ts',
    // karma typescript preprocessor generates a bunch of .ktp.ts which gets picked
    // up by the watch, rinse and repeat
    appWithDefinitions: ['src/**/*.ts', '!src/**/*.ktp.*'],
    integration: 'src/tests/integration/**/*.js',
    index: 'src/index.html'
};
gulp.task('sass', function () {
    return gulp.src(globs.sass)
        .pipe($.sass({style: 'compressed', errLogToConsole: true}))
        .pipe($.autoprefixer())  // defauls to > 1%, last 2 versions, Firefox ESR, Opera 12.1
        .pipe(gulp.dest(destinations.css))
        .pipe(browserSync.reload({stream: true}));
});
gulp.task('install', shell.task([
    'bower install',
    'echo Bower install successfully completed!'
]));
gulp.task('fontello-install', shell.task([
    './node_modules/fontello-cli/bin/fontello-cli --config ./fontello.json --css ./assets/fontello --font ./build/font install',
    'echo Fontello install successfully completed!',
    'gulp fontello'
]));
gulp.task('scss', function() {
        gulp.src('./build/css/**/*', {read: false})
            .pipe(clean());
        gulp.src('./assets/scss/app.scss')
            .pipe(sass({
                includePaths: sassPaths
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(minifycss())
            .pipe(rename({suffix: '.min'}))
            .pipe(changed('build/css'))
            .pipe(gulp.dest('./build/css'));
        gulp.src('./assets/scss/main.scss')
            .pipe(sass({
                includePaths: sassPaths
            }))
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(sass())
            .pipe(minifycss())
            .pipe(rename({suffix: '.min'}))
            .pipe(changed('build/css'))
            .pipe(gulp.dest('./build/css'));
});
gulp.task('scripts-app', function(){
        gulp.src([
                './node_modules/jquery/dist/jquery.js',
                './node_modules/foundation-sites/dist/foundation.js',
                './node_modules/owl.carousel/dist/owl.carousel.js',
                './node_modules/owl.carousel2.thumbs/dist/owl.carousel2.thumbs.js',
                './other_components/materialize-src/js/bin/materialize.js',
                './node_modules/motion-ui/motion-ui.js'
            ])
            .pipe(changed('build/js'))
            .pipe(concat('app.js'))
            .pipe(jsmin())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('./build/js'));

});

gulp.task('scripts-custom', function(){
    gulp.src('./assets/scripts/*.js')
        .pipe(changed('build/js'))
        .pipe(concat('custom.js'))
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./build/js'));
});

gulp.task('fontello', function(){
    gulp.src('./assets/fontello/*')
        .pipe(concat('fontello.scss'))
        .pipe(gulp.dest('./assets/scss'));
});

gulp.task('images', function(){
        gulp.src('./assets/images/**/*')
            .pipe(changed('build/img'))
            .pipe(image({
                pngquant: true,
                optipng: false,
                zopflipng: true,
                advpng: true,
                jpegRecompress: false,
                jpegoptim: true,
                mozjpeg: true,
                gifsicle: true,
                svgo: true
            }))
            .pipe(gulp.dest('./build/img/'));
});

gulp.task('html', function() {
        gulp.src('./build/*.html', {read: false})
        gulp.src('./assets/pages/*.html')
            .pipe(fileinclude({
                prefix: '@@',
                basepath: '@file'
            }))
            .pipe(gulp.dest('./build'));
});

gulp.task('server', function() {
    return gulp.src('./build/')
        .pipe($.webserver({
            host: '127.0.0.1',
            port: 1114,
            livereload: true,
            open: true
        }));
});

gulp.task('clean', function () {
    return gulp.src('./build/**/*', {read: false})
        .pipe(clean());
});

gulp.task('kill', shell.task([
    'killall -9 gulp',
    'echo Gulp killed!'
]));

gulp.task('default', gulp.series('server','html','scss','scripts-app','scripts-custom','images','fontello'));







