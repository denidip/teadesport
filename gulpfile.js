var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var webserver = require('gulp-webserver');
var svg = require('gulp-svg-sprite');



var isDist = $.util.env.type === 'dist';
var assetsFolder =  isDist ? 'dist' : 'assets';
var outputFolder = isDist ? 'dist' : 'build';

var globs = {
    sass: 'node_modules/foundation-sites/scss',
    templates: assetsFolder + '/pages/**/*.html'
};

var destinations = {
    css: outputFolder + "/css",
    themes: outputFolder + "/css/themes",
    js: outputFolder + "/js",
    img: outputFolder + "/img",
    index: outputFolder
};

var assets = {
    sass_app: [
        assetsFolder +'/scss/app.scss',
        assetsFolder +'/scss/_settings.scss'
        ],
    sass_main: [
                assetsFolder +'/scss/main.scss',
                assetsFolder +'/scss/components/*.scss'
                ],
    sass_themes: assetsFolder +'/scss/themes/*.scss',
    js_main: assetsFolder +'/scripts/*.js',
    js_app: [
            './node_modules/foundation-sites/dist/js/foundation.js',
            './node_modules/owl.carousel/dist/owl.carousel.js'
            ],
    jquery: './node_modules/jquery/dist/jquery.js',
    images: assetsFolder +'/images/*.*',
    svg: assetsFolder +'/svg/*.*',
    html: assetsFolder +'/pages/*.html',
    html_watch: assetsFolder +'/pages/**/*.html'
};



gulp.task('sass-app', function () {
    return gulp.src(assets.sass_app)
        .pipe($.sass({style: 'compressed', errLogToConsole: true, includePaths: globs.sass}))
        .pipe($.autoprefixer())  // defauls to > 1%, last 2 versions, Firefox ESR, Opera 12.1
        .pipe($.cleanCss())
        .pipe($.rename({suffix: '.min'}))
        .pipe(gulp.dest(destinations.css))
        ;
});

gulp.task('sass-main', function () {
    return gulp.src(assets.sass_main)
        .pipe($.sass({
            style: 'compressed',
            errLogToConsole: true,
            includePaths: globs.sass
        }))
        .pipe($.autoprefixer())  // defauls to > 1%, last 2 versions, Firefox ESR, Opera 12.1
        .pipe($.cleanCss())
        .pipe($.rename({suffix: '.min'}))
        .pipe(gulp.dest(destinations.css))
        ;
});

gulp.task('sass-themes', function () {
    return gulp.src(assets.sass_themes)
        .pipe($.sass({
            includePaths: globs.sass
        }))
        .pipe($.autoprefixer())  // defauls to > 1%, last 2 versions, Firefox ESR, Opera 12.1
        .pipe(gulp.dest(destinations.themes))
        ;
});

gulp.task('js-app', function () {
    return gulp.src(assets.js_app)
        .pipe($.concat('app.js'))
        .pipe($.jsmin())
        .pipe($.rename({suffix: '.min'}))
        .pipe(gulp.dest(destinations.js))
        ;
});

gulp.task('js-main', function () {
    return gulp.src(assets.js_main)
        .pipe($.concat('main.js'))
        .pipe($.jsmin())
        .pipe($.rename({suffix: '.min'}))
        .pipe(gulp.dest(destinations.js))
        ;
});

gulp.task('jquery', function () {
    return gulp.src(assets.jquery)
        .pipe($.jsmin())
        .pipe($.rename({suffix: '.min'}))
        .pipe(gulp.dest(destinations.js))
        ;
});

gulp.task('images', function () {
    return gulp.src(assets.images)
        .pipe($.image({
            pngquant: true,
            optipng: false,
            zopflipng: true,
            advpng: true,
            jpegRecompress: false,
            jpegoptim: true,
            mozjpeg: true,
            gifsicle: true,
            svgo: true
        }))
        .pipe(gulp.dest(destinations.img))
        ;
});

gulp.task('svg', function(){
    var svgConfig = {
        shape: {
            dimension: {
                maxWidth: 30,
                maxHeight: 30,
                attributes: false
            },
            spacing: {
                padding: 0
            },
            transform: ['svgo']
        },
        svg: {
            xmlDeclaration      : false,
            doctypeDeclaration  : false
        },
        mode: {
            css: false,
            view: false,
            defs: false,
            stack: false,
            symbol: {
                dest: 'svg',
                sprite: 'sprite.svg',
                bust: false,
                example: true
            }
        }
    };
    return gulp.src(assets.svg)
        //.pipe(changed(path.build.img))
        .pipe(svg(svgConfig))
        .pipe(gulp.dest(destinations.img));
});

gulp.task('html', function() {
    return gulp.src(assets.html)
        .pipe($.fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest(destinations.index))

    ;
});

gulp.task('server', function() {
    gulp.src(destinations.index)
        .pipe(webserver({
            livereload: true,
            open: true,
            host: '127.0.0.1',
            port: 8083
        }));
});


gulp.task('watch', function() {
    gulp.watch(assets.sass_app, gulp.series('sass-app'));
    gulp.watch(assets.sass_main, gulp.series('sass-main'));
    gulp.watch(assets.sass_themes, gulp.series('sass-themes'));
    gulp.watch(assets.js_main, gulp.series('js-main'));
    gulp.watch(assets.images, gulp.series('images'));
    gulp.watch(assets.svg, gulp.series('svg'));
    gulp.watch(assets.html_watch, gulp.series('html'));
});




gulp.task('default', gulp.parallel('html','sass-app','sass-main','sass-themes','js-app','js-main','jquery','images','svg','server','watch'));







