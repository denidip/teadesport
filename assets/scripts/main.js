$(document).foundation();

$(window).scroll(function() {
    if ($(window).scrollTop() > 50) {
        $('.navigation.top menu').addClass('scroll');
    } else {
        $('.navigation.top menu').removeClass('scroll');
    }
    if ($(window).scrollTop() > 82) {
        $('header').addClass('scroll');
    } else {
        $('header').removeClass('scroll');
    }
    advantage_scroll();
    scroll_to_top();
});
$(document).ready(function(){
    advantage_scroll();
	
	$(".navigation.top menu").find("a").click(function(){
        var el = $(this).attr('href');
		if(el.substr(0, 2)=='/#') el = el.substr(1);
        $('html').animate({
            scrollTop: $(el).offset().top - $("header").outerHeight() +10}, 2000);
        console.log("good");
        return false;
    });

    $(".scroll_to_top").click(function(){
        var el = $(this).attr('href');
        $('html').animate({
            scrollTop: $(el).offset().top + $("header").outerHeight()}, 2000);
        return false;
    });

//    $('a[href^="#"]').click(function(){
//       var el = $(this).attr('href');
//        $('body').animate({
//            scrollTop: $(el).offset().top - $("header").outerHeight()}, 2000);
//        return false;
//    });

    $("#comments").owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        navText: ['<svg><use xlink:href="#tiny-arrow-right-2"></use></svg>','<svg><use xlink:href="#tiny-arrow-right-2"></use></svg>']
    });
    $("#advantages_slider").owlCarousel({
        items: 3,
        nav: true,
        dots: false,
        loop: true,
        autoplay: true,
        autoplayTimeout: 3600,
        autoplayHoverPause: true,
        smartSpeed: 600,
        navText: ['<svg><use xlink:href="#tiny-arrow-right-2"></use></svg>','<svg><use xlink:href="#tiny-arrow-right-2"></use></svg>']
    });
    $("#catalog_slider").owlCarousel({
        items: 4,
        nav: true,
        dots: false,
        smartSpeed: 600,
        navText: ['<svg><use xlink:href="#tiny-arrow-right-2"></use></svg>','<svg><use xlink:href="#tiny-arrow-right-2"></use></svg>']
    });
    $("#consult_slider").owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        navText: ['<svg><use xlink:href="#tiny-arrow-right-2"></use></svg>','<svg><use xlink:href="#tiny-arrow-right-2"></use></svg>']
    });

    $('#more').click(function() {
        $('#more>.spinner').toggleClass('load');
    });
    titleheight();
    //console.log($('.navigation.top').height());
    //console.log($('header').innerHeight());
    //console.log($('body').height());

});

$( window ).resize(function(){
    titleheight();
    advantage_scroll();
});

advantage_scroll_state = 0;

function titleheight(){
    var title_height = $(window).outerHeight() - $('header').innerHeight();
    var value_height = title_height;
    var value_min_height = $('#toptitle > .column').innerHeight();
    if ($('#title_advantages').length > 0) {
        var advantages_height = $('#title_advantages > .columns').outerHeight();
        value_height = title_height - advantages_height - 200;
    } else {
        value_height = title_height - 100 - 200;
    }
    $('#toptitle').css('height', value_height).css('min-height', value_min_height);
}
function scroll_to_top() {
    if ($(window).scrollTop() >= $(document).outerHeight()-$(window).outerHeight()-175) {
        $('.scroll_to_top').css('bottom','250px');
    }else{
        if ($(window).scrollTop() >= $(window).outerHeight()) {
            $('.scroll_to_top').addClass('active');
            if (advantage_scroll_state === 0){
                $('.scroll_to_top').css('bottom','50px');
            }else{
                $('.scroll_to_top').css('bottom','200px');
            }
        }else{
            $('.scroll_to_top').removeClass('active');
        }
    }
}
function advantage_scroll() {

    if ($('.advantages_slider').length > 0) {
        var screenheight = $(window).outerHeight();
        var start_point = $('#start_point');
        var stop_point = $('#stop_point');
        var start_ident = start_point.data("ident");
        var stop_ident = stop_point.data("ident");
        var states_1 = 0;
        var states_2 = 0;
        var strat_advantages = start_point.position().top - screenheight + start_ident;
        var stop_advantages = stop_point.position().top - screenheight + stop_ident;
        console.log('Высота окна: '+$(window).outerHeight());
        console.log('Высота страницы: '+$(document).outerHeight());
        console.log('Начало скрола: '+(strat_advantages + start_point.data("ident")));
        console.log('Конец скрола: '+stop_advantages);
        console.log('Текущий скрол: '+$(window).scrollTop());
        //var stop_advantages = strat_advantages + $('.catalog .cards').height() - 410;
        if ($(window).scrollTop() >= strat_advantages){
            states_1 = 1;
        }else{
            states_1 = 0;
        }
        if ($(window).scrollTop() <= stop_advantages){
            states_2 = 1;
        }else{
            states_2 = 0;
        }
        if (states_1 === 1 && states_2 === 1) {
            $('.advantages_slider').addClass('scroll');
            advantage_scroll_state = 1;
        }else{
            $('.advantages_slider').removeClass('scroll');
            advantage_scroll_state = 0;
        }
    }

}